from django import template 
from rango.models import Category


register = template.Library()
@register.inclusion_tag('rango/cats.html')
def get_category_list(cat=None):
    return {'cats': Category.objects.all(), 'act_cat': cat}


def get_category_list(max_results=0,starts_with=''):

	cat_list=[]
	print "get_category_list"
	print starts_with
	if starts_with:
		print "inside if "
		cat_list = Category.objects.filter(name__istartswith=starts_with)
		print "after filter"
	else:
		cat_list = Category.objects.all()
	if max_results>0:
		if (len(cat_list)) > max_results:
			cat_list = cat_list[:max_results]
	print "after updating category"
	print cat_list
	return cat_list



