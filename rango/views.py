from django.shortcuts import render
from django.http import HttpResponse
from rango.models import Category,Page
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from rango.forms import CategoryForm , PageForm , UserForm , UserProfileForm , LoginForm
from datetime import datetime
from rango.bing_search import run_query
from django.shortcuts import redirect
from rango.templatetags.rango_extras import get_category_list

# Create your views here.


def index(request):
	category_list = Category.objects.order_by('-likes')[:5]
	context_dict = {}
	context_dict['categories']=category_list
	pages_list = Page.objects.order_by('-views')[:5]
	context_dict['pages']=pages_list
	visits = request.session.get('visits')
	if not visits:
		visits =1 
	reset_last_visit_time = False
	last_visit=request.session.get('last_visit')
	if last_visit:
		last_visit_time =datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")
		if (datetime.now() - last_visit_time).seconds > 0 :
			visits = visits + 1
			reset_last_visit_time = True
	else:
		print "else part setting it for the first time "
		reset_last_visit_time = True
	if reset_last_visit_time:
		print "updating value "
		request.session['last_visit']=str(datetime.now())
		request.session['visits']=visits
	context_dict['visits'] = visits
	response = render(request, 'rango/index.html', context_dict)
	

		






	return response

def about(request):
	context_dict = {'boldmessage' : 'accessing about page'}
	visits = int(request.COOKIES.get('visits' , '1'))
	reset_last_visit_time = False
	context_dict['visits']=visits
	response = render(request,'rango/about.html',context_dict)


	if 'last_visit' in request.COOKIES:
		last_visit = request.COOKIES['last_visit']
		last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")
		if (datetime.now() - last_visit_time).seconds > 0:
			reset_last_visit_time = True
			visits = visits +1
			context_dict['visits']=visits
			response = render(request,'rango/about.html',context_dict)
	else:
		reset_last_visit_time = True
	if reset_last_visit_time:
		response.set_cookie('visits',visits)
		response.set_cookie('last_visit',datetime.now())
		

	return response

def category(request,category_name_slug):
	print "inside category"
	context_dict = {}
	context_dict['result_list'] = None
	context_dict['query'] = None
	try:

		category = Category.objects.get(slug=category_name_slug)
		context_dict['category_name']=category.name
		pages = Page.objects.filter(category=category)
		context_dict['pages']=pages
		context_dict['category']=category
		print "after data"
	except Category.DoesNotExist:
		pass

	if request.method=='POST':
		print "inside post"
		query = request.POST.get('query', False)
		print "after post"
		if query:

			result_list = run_query(query)
			context_dict['result_list']=result_list
			context_dict['query'] = query

	print "before render"
	return render(request, 'rango/category.html', context_dict)
@login_required
def add_category(request):
	if request.method == 'POST':
		form = CategoryForm(request.POST)
		if form.is_valid():
			form.save(commit=True)
			return index(request)
		else:

			print form.errors


	else:

		form = CategoryForm()


	return render(request,'rango/add_category.html',{'form':form})
@login_required
def add_page(request,category_name_slug):

	cate = Category.objects.get(slug=category_name_slug)
	if request.method == 'POST':

		form = PageForm(request.POST)
		if form.is_valid():
			print "before form save "
			page=form.save(commit=False)
			print "after for save"
			page.category=cate
			page.save()
			print "after saving"
			return category(request,category_name_slug)
		else:
			print form.errors
	else:

		form = PageForm()

	context_dict={}
	context_dict['form']=form
	context_dict['category']=cate


	return render(request,'rango/add_page.html',context_dict)

def register(request):
	print "****inside register method****"
	context_dict={}
	registered = False
	if request.session.test_cookie_worked():
		print ">>>> TEST COOKIE WORKED!"
		request.session.delete_test_cookie()
	if request.method =='POST':
		print "****inside post method****"
		user_form = UserForm(request.POST)
		profile_form = UserProfileForm(request.POST)
		if user_form.is_valid():
			user = user_form.save()
			user.set_password(user.password)
			user.save()
			profile = profile_form.save(commit=False)
			profile.user = user 
			if 'picture' in request.FILES:
			 	profile.picture = request.FILES['picture']
			profile.save()
			registered = True
			print "***registered***"

		else:
			print user_form.errors , profile_form.errors
	else:

		user_form = UserForm()
		profile_form = UserProfileForm()
		context_dict['user_form']=user_form
		context_dict['profile_form']=profile_form
		context_dict['registered']= False

	return render(request,'rango/register.html',context_dict)

def user_login(request):
	
	if request.method=='POST':
		
		print "post method"
		username = request.POST.get('username')
		password = request.POST.get('password')
		print username
		print password
		user= authenticate(username=username,password=password)
		if user:
			if user.is_active:
				 login(request,user)
				 print '**** beofre renfering index'
				 return HttpResponseRedirect('/rango/')
			else:
				return HttpResponse("Your Rango account is disabled.")
				print "your account is disabled"
		else:
			  print "Invalid login details: {0}, {1}".format(username, password)
			  return HttpResponse("Invalid login details supplied.")

		
		
	return render(request,'rango/login.html',{})
@login_required
def restricted(request):
	return render(request,'rango/restricted.html')
@login_required
def user_logout(request):
	logout(request)
	return HttpResponseRedirect('/rango/')
def search(request):
	result_list = []
	if request.method == 'POST':
		query = request.POST['query'].strip()
		if query:
			result_list = run_query(query)


	return render(request, 'rango/search.html', {'result_list': result_list})
def track_url(request):
	print "***inside track_url"
	page_id=None
	url = '/rango/'
	if request.method=='GET':
		print "inside get "
		print request.GET
		print "inside page_id"
		page_id = request.GET['page_id']
		print page_id
		try:
			page = Page.objects.get(id=page_id)
			page.views = page.views+1
			page.save()
			url=page.url
		except:
			pass
	return redirect(url)

def register_profile(request):

	context_dict={}
	if request.method=='POST':
		profile_form = UserProfileForm(request.POST)
		if profile_form.is_valid():
			print "inside profile valid "
			current_user = request.user
			print request.user
			profile=profile_form.save(commit=False)
			profile.user = current_user 
			if 'picture' in request.FILES:
				profile.picture = request.FILES['picture']
			profile.save()



		print "post method" 
	else:

		profile_form = UserProfileForm()
		context_dict['profile_form']=profile_form


	print "inside register_profile"
	return render(request,'rango/profile_registration.html',context_dict)
@login_required
def like_category(request):
	print "inside likes"
	cat_id=None
	print request.GET
	if request.method == 'GET':
		print "cat_id before "
		cat_id=request.GET['category_id']
		print "cat_id after "
	likes=0
	print cat_id
	print likes
	if cat_id:
		cat = Category.objects.get(id=int(cat_id))
		print cat
		if cat:
			print cat.likes
			likes = cat.likes + 1
			cat.likes =  likes
			cat.save()
	return HttpResponse(likes)

def suggest_category(request):
	cat_list =[]
	starts_with = ''
	print request.GET['suggestion']
	if request.method == 'GET':
		print "inside if"
		starts_with = request.GET['suggestion']
	print "beofre calling"
	cat_list = get_category_list(8, starts_with)
	print "after calling"
	print cat_list

	return render(request,'rango/cats.html',{'cats': cat_list })